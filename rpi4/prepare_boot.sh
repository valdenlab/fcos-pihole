# For now copy paste these commands as necessary, and don't forget to double check your block device labes for example with "lsblk"

# Target device for fcos install. For example check with "lsblk"
FCOSDISK=${1:-'/dev/sda'}
IGNITION_FILE=${2:-'prod.ign'}

[ ! -d "${PWD}/efi" ] && 
wget -O /tmp/z.$$ https://gitlab.com/valdenlab/fcos-pihole/-/jobs/artifacts/main/download?job=build_boot_rpms && 
   sudo unzip -d ${PWD} /tmp/z.$$ &&
   rm /tmp/z.$$

# For providing fcos image locally
#USBDISK=/dev/sdb
#USBIMG=fedora-coreos-36.20221001.3.0-metal.aarch64.raw.xz
#sudo mkdir -p /mnt/usb 
#sudo mount ${USBDISK}1 /mnt/usb/

#sudo podman run --pull=always --privileged --rm \
#    -v /dev:/dev -v /run/udev:/run/udev -v .:/data -v /mnt/usb:/tmp/ -w /data \
#    quay.io/coreos/coreos-installer:release \
#    install $FCOSDISK -i pihole1_init.ign -f /tmp/$USBIMG --insecure

# Download fcos image
sudo podman run --pull=always --privileged --rm \
    -v /dev:/dev -v /run/udev:/run/udev -v .:/data -w /data \
    quay.io/coreos/coreos-installer:release \
    install $FCOSDISK -i $IGNITION_FILE


FCOSEFIPARTITION=$(lsblk $FCOSDISK -J -oLABEL,PATH  | jq -r '.blockdevices[] | select(.label == "EFI-SYSTEM")'.path) && 
    mkdir -p /tmp/FCOSEFIpart &&
    sudo mount $FCOSEFIPARTITION /tmp/FCOSEFIpart &&
    sudo rsync -avh --ignore-existing ${PWD}/efi/ /tmp/FCOSEFIpart/ &&
    sudo umount $FCOSEFIPARTITION

sudo eject $FCOSDISK

