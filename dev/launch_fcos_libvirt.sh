#!/bin/bash
images=(${PWD}/.images/*)
IMAGE=${images[0]}
IGNITION_CONFIG="${PWD}/.ignition/dev.ign"
VM_NAME="dev-fcos"
VCPUS="2"
RAM_MB="2048"
STREAM="stable"
DISK_GB="10"

virsh destroy dev-fcos || true
virsh undefine --remove-all-storage dev-fcos  || true
virt-install --name="${VM_NAME}" --vcpus="${VCPUS}" --memory="${RAM_MB}" \
        --os-variant="fedora-coreos-$STREAM" --import --graphics=none \
        --disk="size=${DISK_GB},backing_store=${IMAGE}" \
        --network bridge=virbr0 \
        --qemu-commandline="-fw_cfg name=opt/com.coreos/config,file=${IGNITION_CONFIG}"