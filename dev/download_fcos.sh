#!/bin/bash

STREAM="stable"

sudo mkdir -p $(pwd)/.images
sudo rm .images/*

sudo podman run --pull=always --rm -v $(pwd)/.images/:/data -w /data \
    quay.io/coreos/coreos-installer:release download -s "${STREAM}" -p qemu -f qcow2.xz --decompress