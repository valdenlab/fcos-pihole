#!/bin/bash
mkdir -p ${PWD}/.ignition/butane

MAIN_FILE=${1:-'dev'}

# Files in butane directory have to be transpiled first, so they can be used with the merge-local directive,
# because the merge-local encodes the source files as is.
# Output file extension will be .bu despite the contents being ignition, 
# which improves readability of the main merging butane config.
find ./butane -mindepth 1 -exec podman run --interactive --rm --security-opt label=disable \
       --volume ${PWD}:/pwd --workdir /pwd quay.io/coreos/butane:release \
       --pretty --strict --output ./.ignition/{} {} \;

find ./.ignition/butane/ -mindepth 1 -exec podman run \
       --volume ${PWD}:/pwd --workdir /pwd --rm -i \
       quay.io/coreos/ignition-validate:release {} \;

podman run --interactive --rm --security-opt label=disable \
       --volume ${PWD}:/pwd --workdir /pwd quay.io/coreos/butane:release \
       --pretty --strict --files-dir .ignition/butane $MAIN_FILE.bu > ${PWD}/.ignition/$MAIN_FILE.ign

podman run --rm -i quay.io/coreos/ignition-validate:release - < ${PWD}/.ignition/$MAIN_FILE.ign