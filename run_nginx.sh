# Run after domain creation to enable access to pihole admin panel. Reverse proxy is needed for virtualized dev environments such as WSL or Crostini,
# where accessing the virtual interface from the "host-host" is not directly possible.
# For example with Crostini, a simple localhost:8080/admin seems to work from the ChromeOS browser
DOMAIN_IFADDR=`virsh domifaddr --source arp dev-fcos | grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"`
echo $DOMAIN_IFADDR
podman run --rm --name nginx-dev -e FCOS_IP=$DOMAIN_IFADDR -e NGINX_ENVSUBST_OUTPUT_DIR=/etc/nginx -v ${PWD}/dev/nginx:/etc/nginx/templates -p 8080:80 docker.io/library/nginx:latest