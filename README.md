# fcos-pihole

The goal of this project was to experiment with [Fedora CoreOS](https://docs.fedoraproject.org/en-US/fedora-coreos/getting-started/), while also producing something actually useful in the form of a network wide ad-blocker via [Pihole](https://pi-hole.net/). What mainly got me interested in FCOS was the auto updating features, and together with easily reproducible Ignition deployments, make FCOS a great pick for such a simple fire-and-forget deployment, with minimal maintenance required.

Most time was spent on forming a good developement workflow with the help of virtualization, and figuring out the process for preparing the Raspberry Pi boot disks. The actual FCOS and Pihole sides of this little project were quite straightforward to implement. Now after a few weeks of "production" use, I could say I'm very pleased and would highly recommend this setup.


## Development environment
I personally use Fedora 36 in both Crostini and WSL. This allows me to seamlessly switch between my Chromebook and Windows workstation.

After setting up your Fedora box, system container or whatever fits your situation at hand,
you will also need to install some basic tools such as `podman` and `git` into it,
and clone this repository to start developing:
```bash
dnf upgrade -y
dnf install -y git podman
git clone https://gitlab.com/valdenlab/fcos-pihole.git
```

### Running Fedora CoreOS in Fedora
Fedora provides this nice simple setup for `libvirt`: [Getting started with virtualization](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/)
Basically all you need for running the `run_dev.sh` script in terms of virtualization capabilites is:
```bash
sudo dnf install @virtualization
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
```
Now that you have virtualization set up, you need to pre-download the fcos image with
```bash
bash dev/download_fcos.sh
```
Now you're ready to run the developement environment! The starting point of your config is `dev.bu` at the repository root.
```bash
# Optional for seeing config errors more easily before running the whole thing. This step is included in run_dev.sh
bash generate_ignition.sh

# Main script
bash run_dev.sh

# Optional for accessing the pihole admin panel in a nested virtualization environment. Url: localhost:8080/admin
# Either exit the libvirt console, or run in a seperate shell
bash run_nginx.sh
```
**Note:** Detaching from `virsh console` has its own hotkey. In my case it was `ctrl+å` in Crostini and `ctrl+¨` in WSL. YMMV depending on keyboard layout.

### References
* [Install Fedora 36 on WSL](https://dev.to/bowmanjd/install-fedora-on-windows-subsystem-for-linux-wsl-4b26)
* [Integrate Fedora 36 into Chrome OS - see relevant commands from description](https://youtu.be/Pwki5YyHaDo)
* [Producing an Ignition Config](https://docs.fedoraproject.org/en-US/fedora-coreos/producing-ign/)
* [Provisioning Fedora CoreOS on libvirt](https://docs.fedoraproject.org/en-US/fedora-coreos/provisioning-libvirt/)

## Raspberry Pi
Provisioining to bare metal has a bit more considerations to make. I use PiOS on a Raspberry Pi 3 to prepare my boot SD cards.
I'm also using the Windows version of the Raspberry Imager software for bootloader updates.

In my opinion you should be very conscious on your decisions when preparing such a "production" installation, so I'm going to keep the following list short and simple.

1. Update EEPROM for the RPi4s
2. Have a bare metal linux installation that makes it possible to write filesystems to block devices such as `/dev/sda`
3. Clone this repo on your boot preparer
4. `cp prod.bu.template pihole_prod.bu` and fill necessary `TODO` fields; generate ssh keys etc.
5. Check the location of the plugged in SD card i.e. with `lsbkl`. Example value `/dev/sda`
6. `bash generate_ignition pihole_prod`
7. `bash rpi4/prepare_boot.sh /dev/sda .ignition/pihole_prod.ign`
8. Plug the SD card into your target device and boot. **Tip:** Plug in a monitor to see possible errors. 

### References
* [Provisioning Fedora CoreOS on the Raspberry Pi 4](https://docs.fedoraproject.org/en-US/fedora-coreos/provisioning-raspberry-pi4/)

I made a pipeline that prepares the boot rpms required by RPi4. These artifacts are used by the `prepare_boot.sh`:
* [Packages for FCOS boot on Raspberry Pi 4](../-/jobs/artifacts/main/download?job=build_boot_rpms)